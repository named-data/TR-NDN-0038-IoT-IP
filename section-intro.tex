\section{Overview}
\label{sec:intro}

``Internet of Things'' (IoT) generally refers to the interconnection of different types of computing devices to support various kinds of monitoring and control applications.  To accommodate the heterogeneity of devices and applications from different venders, modern IoT systems have adopted the open standards of TCP/IP protocol suite, which was developed for the wired global Internet several decades ago, as the networking solution.
However, IoT networks differ from traditional wired computer networks in fundamental ways as we elaborate below.
%due to various constraints on the device hardware, the link layer technology, and the supported applications.
Those differences pose significant challenges in applying TCP/IP technologies to the IoT environment, and addressing these challenges will make a far-reaching impact on the network architecture.  This paper aims to systematically identify the challenges posed by the IoT environment, and to articulate the future direction to tackle the challenges. 

IoT networks often contain a large number of low-end, resource-constrained devices.
% RFC 5548 ``Routing Requirements for Urban Low-Power and Lossy Networks''~\cite{rfc5548} states that typical IoT deployment in the urban environment may contain $10^2$ to $10^7$ sensing nodes.
The design of those devices are mostly driven by low manufacturing and operational cost.
As a result, the IoT devices are typically equipped with limited computing power and required to operate over long time periods (e.g., a year) on battery.
Due to the power constraints, the IoT networks often employ low-energy Layer-2 technologies, such as IEEE 802.15.4, Bluetooth LE and low-power Wi-Fi, which usually operate with much smaller MTU and lower transmission rate compared to traditional Ethernet links.
Therefore an immediate challenge for the IoT network protocol design is to adapt the packet size to the constrained links (discussed in Section~\ref{sec:mtu}).
To save energy, IoT nodes may not be always on as in wired networks.
Moreover, an IoT system may be deployed in environments without wired network infrastructure (e.g., forests, underwater, battle fields) and consequently has to rely on wireless mesh technologies to communicate.
% The IETF has defined a multi-link subnet architecture, with an intra-network (sometimes called \emph{route-over}) routing protocol, for IPv6 over a mesh network to be treated as a single subnet.~\cite{rfc6606}%
% \footnote{An alternative called \emph{mesh-under} provides Layer-2 packet forwarding, hiding the multiple radio links from IPv6.}
This brings more challenges to the TCP/IP protocol architecture:
first, mesh networks typically adopt the multi-link subnet model which is not supported by the original IP addressing architecture (discussed in Section~\ref{sec:subnet});
second, broadcast and multicast are expensive on a battery powered network as a single multicast will involve a series of multi-hop forwarding and potentially wake up many sleeping nodes (discussed in Section~\ref{sec:mcast});
third, a scalable routing mechanism is now necessary for IP communications to happen over the mesh networks (discussed in Section~\ref{sec:routing});
and lastly, the TCP-style reliable and in-order byte stream delivery is often ill-suited for applications that require customized control and prioritization of their data (discussed in Section~\ref{sec:tcp}).

% Note that there are also wired technologies such as BACnet~\cite{bacnet} whose characteristics have an effect on the design of TCP/IP, because of MTU limits, bandwidth constraints, or lack of broadcast/multicast support.
% Even those technologies that closely meet the expectations of the TCP/IP protocols may have constrained devices with limited resources and power budget, so that the TCP/IP protocols are not well-adapted to the those technologies.

Most IoT applications interact with lots of sensors and actuators to perform various monitoring and control tasks on the ambient environment.
Their design patterns intrinsically require efficient and scalable support for naming configuration and discovery, security protection on the data acquisition and actuation operations, and a resource-oriented communication interface such as \emph{Representational State Transfer} (REST).
Unfortunately, existing solutions to those problems, many of which are widely used by today's Web technologies, do not satisfy the constraints of the IoT environments.
For example, the traditional DNS-based naming services are unsuitable in many IoT deployment scenarios that lack infrastructural support for dedicated servers (see Section~\ref{sec:disc}).
The application-layer content caches and proxies are often inefficient in dynamic network environments with intermittent connectivity (discussed in Section~\ref{sec:cache}).
In addition, the channel-based security protocols such as TLS and DTLS, which are used to secure the REST communications, impose high overhead on the IoT devices in terms of protocol operations and resource consumption (discussed in Section~\ref{sec:security}).


The rest of this paper discusses each of the aforementioned issues in detail.
We seek to identify the architectural reason that causes the difficulties when applying TCP/IP to the IoT world.
We also survey the current solutions to those issues that have been standardized or under active development at the IETF, and analyze why they are often insufficient to solve the targeted problems.
The goal of this paper is to offer insights and point out directions for the design of future IoT network architectures.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "IOT-IP"
%%% End:
