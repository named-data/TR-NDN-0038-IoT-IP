\section{Rethinking the Architecture}
\label{sec:arch}

The famous principle of indirection says that \emph{``all problems in computer science can be solved by another level of indirection''}.
But one problem it does not solve is the existence of \emph{too many levels of indirection}, which precisely describes the situation of the current IoT network architecture.

Figure~\ref{fig:arch} shows the layered structure of an IP-based IoT stack.
To support the \emph{REST} interface, IoT applications usually adopt CoAP or HTTP as the messaging protocol.
Usually the applications also need to interact with common services on top of the messaging layer (such as the CoAP Resource Directory and object security support).
Right above the transport layer, TLS and DTLS are added to secure the communication channel.
In addition, there are multiple infrastructural services that are necessary to facilitate the IP network communications, such as ICMP, DHCP, Neighbor Discovery (ND), DNS and RPL.

%This architectural complexity eventually leads to inflated code size (for both source code and compiled executables), increased memory footprint (for maintaining various internal states required by different protocols), and high computational cost (for executing different protocol operations).

\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth, keepaspectratio=true]{figs/IP-IoT-arch.pdf}
\caption{A typical architecture for IoT systems}
\label{fig:arch}
\end{figure}


If we reexamine the network stack by focusing on the core functionalities from the application's perspective, we will get a rather different picture shown in Figure~\ref{fig:arch-app}.
Instead of \emph{``everything over IP''}, the IoT applications have converged on a different paradigm of \emph{``everything over REST''}.
At the bottom, an IoT stack may use any data transport such as UDP and 6LoWPAN.
In the center of the stack, a \emph{RESTful} messaging protocol implements all the service components that operate over a single abstraction of the \emph{application data unit} (ADU) defined by the IoT applications.
The contrast between this new perspective and the layered view of the existing stack reflects the deep-rooted mismatch between the expectations from the IoT applications and the architectural reality of TCP/IP.

\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth, keepaspectratio=true]{figs/IoT-app-arch.pdf}
\caption{An IoT stack from the application's perspective}
\label{fig:arch-app}
\end{figure}


The \emph{REST} layer contains several sub-modules that implement critical functionalities:

\begin{itemize}
\item a URI-based communication mechanism that can deliver application-layer data to network destinations;
\item a caching mechanism for efficient data dissemination;
\item an object security mechanism for protecting the integrity and confidentiality of individual ADUs;
\item a congestion control module that may implement multiple algorithms for different network environments;
\item naming configuration and resource discovery for assisting the application operations;
\item a sequencing mechanism for chopping large data that cannot fit into a single ADU;
\item a reliability mechanism that supports packet retransmission and ordering according to the application's demand.
\end{itemize}


Currently all those functionalities (including the \emph{REST} interface itself) are implemented by the application layer protocols.
However, some of those functionalities could have been more effective if moved into the core network.
For example, the congestion control could benefit from the feedbacks of network and link layers to make wiser decisions.
Caching could be more efficient if the caches are ubiquitous inside the network, rather than relying on dedicated caching proxies.
%Caching can also help provide scalable and efficient multicast solutions like MPL (see Section~\ref{sec:mcast}).
To utilize in-network caching, URI-based forwarding, \emph{REST} interface and object security should also be supported at the network layer so that the cached content can be easily located, retrieved and authenticated.
This protocol stack optimization eventually lead to a simpler and more efficient architecture that closely resembles the \emph{Information-Centric Network} (ICN) vision.

The ICN architectures such as NDN~\cite{ccn, ndn} not only provide native support for the functionalities that IoT applications intrinsically demand, but also address the lower-layer network challenges.
It applies the same ADU across layers and gives the packet flow control back to the applications.
It does not have artificial requirements on minimum MTU; the simplified stack actually reduces the size of packet headers.
It is inherently multicast friendly since pervasive caching allows data to be reused by multiple consumers efficiently.
Its data oriented communication avoids the issue of addressing and routing to a large number of sensor nodes and opens the opportunity for scalable routing and forwarding over application layer names.
The data centric security avoids the overhead entailed by the channel-based security solutions and better suits the IoT devices with limited resources and intermittent connectivity.
The architectural simplicity leads to smaller code size for the application software, lower energy and memory footprint for the device, and better utilization of the network resource compared to the current IP-based IoT stack.
The potentials of IoT over ICN have already drawn attention at the IRTF \emph{icnrg}~\cite{iot-icn} and we expect it to become an active research topic as the interest in the IoT technologies continues to grow.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "IOT-IP"
%%% End:
